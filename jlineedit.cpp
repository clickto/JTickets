#include "jlineedit.h"

JLineEdit::JLineEdit(QLineEdit *parent) :
    QLineEdit(parent)
{
}


void JLineEdit::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        //触发clicked信号
        emit click();
    }
    //将该事件传给父类处理
    QLineEdit::mousePressEvent(event);
}
