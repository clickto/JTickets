﻿#include "trainticket.h"
#include "ui_trainticket.h"

#include <QDebug>
#include <QMessageBox>

TrainTicket::TrainTicket(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TrainTicket)
{
    ui->setupUi(this);

    Init();
}

TrainTicket::~TrainTicket()
{
    TicketsList.clear();
    StationMap.clear();
    StationList.clear();
    for(int i = 0; i < pBtnListOrder.size(); i++) {
        if(pBtnListOrder.at(i) != NULL) {
            delete pBtnListOrder.at(i);
            pBtnListOrder.at(i) = NULL;
        }
    }
    pBtnListOrder.clear();

    delete LogInOut;
    delete ToolBar;
    delete Manage;
    delete LbSrc;
    delete pBtnExchange;
    delete LbDest;
    delete cBoxDest;
    delete LbDate;
    delete pBtnSubDate;
    delete LEdtDate;
    delete pBtnAddDate;
    delete HLayInfo;
    delete LbSeat;
    delete kBoxAllSeat;
    delete kBoxBussiness;
    delete kBoxFrist;
    delete kBoxSecond;
    delete kBoxHSoftSeat;
    delete kBoxSoftPer;
    delete kBoxDSoftper;
    delete kBoxYPer;
    delete kBoxSoftSeat;
    delete kBoxSeat;
    delete kBoxStand;
    delete kBoxOther;
    delete HLaySeat;
    delete VLayInfo;
    delete LineSep;
    delete kBoxAdult;
    delete kBoxStudent;
    delete kBoxChild;
    delete LEdtChildCnt;
    delete pBtnLeftTickets;
    delete GLayQuery;
    delete HLayOperat;
    delete LbStaticInfo;
    delete LbRTInfo;
    delete TableTrainInfo;
    delete VLayTrainInfo;
    delete TabRobTicket;
    delete ui;
}

void TrainTicket::Init()
{
    // 初始化最大化显示
    this->setWindowState(Qt::WindowMaximized);
    // 初始化网络
    Manage = new QNetworkAccessManager(this);
    connect(Manage, SIGNAL(finished(QNetworkReply*)), this, SLOT(ReceiveNetworkInfo(QNetworkReply*)));
    // 初始化工具栏
    InitToolBar();
    // 初始化控件
    InitCtrls();

    User.clear();
    Passwd.clear();
    IsCalenShow = false;
    StationMap.clear();
    StationList.clear();
    IsLog = false;

    // 请求站点信息
    QString url = "https://kyfw.12306.cn/otn/resources/js/framework/station_name.js";
    Manage->get(QNetworkRequest(QUrl(url)));
}

void TrainTicket::InitToolBar()
{
    ToolBar = new QToolBar(this);
    LogInOut = new QAction(QStringLiteral("登 录"), this);
    ToolBar->addAction(LogInOut);
    connect(LogInOut, SIGNAL(triggered()), this, SLOT(LogInOutSlot()));
    ToolBar->show();
    MainWin = new QVBoxLayout(this);
    MainWin->addWidget(ToolBar);
    MainWin->addWidget(ui->tabWidget);
}

void TrainTicket::InitCtrls()
{
    LbSrc = new QLabel(QStringLiteral("出发:"));
    cBoxSrc = new QComboBox;
    pBtnExchange = new QPushButton("<->");
    LbDest = new QLabel(QStringLiteral("目的:"));
    cBoxDest = new QComboBox;
    LbDate = new QLabel(QStringLiteral("日期:"));
    pBtnSubDate = new QPushButton("<");
    LEdtDate = new JLineEdit;
    Calend = new QCalendarWidget;
    pBtnAddDate = new QPushButton(">");
    HLayInfo = new QHBoxLayout;
    // 出发站名
    connect(cBoxSrc, SIGNAL(currentTextChanged(QString)), this, SLOT(CheckStartStationName(QString)));
    // 终点站站名
    connect(cBoxDest, SIGNAL(currentTextChanged(QString)), this, SLOT(CheckEndStationName(QString)));
    // 日期控件槽函数
    connect(LEdtDate, SIGNAL(click()), this, SLOT(CalenderWidget()));
    // 日历控件无边框
    Calend->setWindowFlags(Qt::FramelessWindowHint);
    // 日历控件槽函数
    connect(Calend, SIGNAL(selectionChanged()), this, SLOT(ReceiveCalenDate()));
    // 初始化日期
    LEdtDate->setText(QDateTime::currentDateTime().toString("yyyy-MM-dd"));
    // 出发的/目的地
    cBoxSrc->setEditable(true);
    cBoxDest->setEditable(true);
    HLayInfo->addWidget(LbSrc);
    HLayInfo->addWidget(cBoxSrc);
    HLayInfo->addWidget(pBtnExchange);
    HLayInfo->addWidget(LbDest);
    HLayInfo->addWidget(cBoxDest);
    HLayInfo->addWidget(LbDate);
    HLayInfo->addWidget(pBtnSubDate);
    HLayInfo->addWidget(LEdtDate);
    HLayInfo->addWidget(pBtnAddDate);
    HLayInfo->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Expanding));
    HLayInfo->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Expanding));
    HLayInfo->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Expanding));

    LbSeat = new QLabel(QStringLiteral("隐藏:"));
    kBoxAllSeat = new QCheckBox(QStringLiteral("全选"));
    kBoxBussiness = new QCheckBox(QStringLiteral("商务座"));
    kBoxFrist = new QCheckBox(QStringLiteral("一等座"));
    kBoxSecond = new QCheckBox(QStringLiteral("二等座"));
    kBoxHSoftSeat = new QCheckBox(QStringLiteral("高软"));
    kBoxSoftPer = new QCheckBox(QStringLiteral("软卧"));
    kBoxDSoftper = new QCheckBox(QStringLiteral("动卧"));
    kBoxYPer = new QCheckBox(QStringLiteral("硬卧"));
    kBoxSoftSeat = new QCheckBox(QStringLiteral("软座"));
    kBoxSeat = new QCheckBox(QStringLiteral("硬座"));
    kBoxStand = new QCheckBox(QStringLiteral("无座"));
    kBoxOther = new QCheckBox(QStringLiteral("其他"));
    HLaySeat = new QHBoxLayout;
    VLayInfo = new QVBoxLayout;
    HLaySeat->addWidget(LbSeat);
    HLaySeat->addWidget(kBoxAllSeat);
    HLaySeat->addWidget(kBoxBussiness);
    HLaySeat->addWidget(kBoxFrist);
    HLaySeat->addWidget(kBoxSecond);
    HLaySeat->addWidget(kBoxHSoftSeat);
    HLaySeat->addWidget(kBoxSoftPer);
    HLaySeat->addWidget(kBoxDSoftper);
    HLaySeat->addWidget(kBoxYPer);
    HLaySeat->addWidget(kBoxSoftSeat);
    HLaySeat->addWidget(kBoxSeat);
    HLaySeat->addWidget(kBoxStand);
    HLaySeat->addWidget(kBoxOther);
    HLaySeat->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Expanding));
    VLayInfo->addLayout(HLayInfo);
    VLayInfo->addLayout(HLaySeat);
    kBoxAllSeat->setChecked(true);
    IsSeatCheck(true);
    connect(kBoxAllSeat, SIGNAL(clicked()), this, SLOT(AllSeatClick()));

    LineSep = new QFrame;
    kBoxAdult = new QCheckBox(QStringLiteral("成人"));
    kBoxStudent = new QCheckBox(QStringLiteral("学生"));
    kBoxChild = new QCheckBox(QStringLiteral("儿童"));
    LEdtChildCnt = new QLineEdit;
    pBtnLeftTickets = new QPushButton(QStringLiteral("查询"));
    GLayQuery = new QGridLayout;
    HLayOperat = new QHBoxLayout;
    LineSep->setGeometry(QRect(40, 180, 400, 3));
    LineSep->setFrameShape(QFrame::VLine);
    LineSep->setFrameShadow(QFrame::Sunken);
    LineSep->raise();
    kBoxAdult->setChecked(true);
    // 查询按钮
    connect(pBtnLeftTickets, SIGNAL(clicked()), this, SLOT(LeftTicketClick()));
    // 查询按钮样式
    pBtnLeftTickets->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    GLayQuery->addWidget(LineSep, 0, 0, 2, 1);
    GLayQuery->addWidget(kBoxAdult, 0, 1, 1, 1);
    GLayQuery->addWidget(kBoxStudent, 0, 2, 1, 1);
    GLayQuery->addWidget(kBoxChild, 1, 1, 1, 1);
    GLayQuery->addWidget(LEdtChildCnt, 1, 2, 1, 1);
    GLayQuery->addWidget(pBtnLeftTickets, 0, 3, 2, 1);
    GLayQuery->setColumnStretch(0, 1);
    GLayQuery->setColumnStretch(1, 1);
    GLayQuery->setColumnStretch(2, 1);
    HLayOperat->addLayout(VLayInfo);
    HLayOperat->addLayout(GLayQuery);
    HLayOperat->setStretch(0, 5);
    HLayOperat->setStretch(1, 1);

    LbStaticInfo = new QLabel(QStringLiteral("有:余票充足； 无:无票； *：未到起售时间； --：无此席位  |  学生票售票时间：12月1号 - 3月31号， 6月1号 - 9月30号"));
    // 设置字体颜色
    QPalette Pale;
    Pale.setColor(QPalette::WindowText, Qt::blue);
    LbStaticInfo->setPalette(Pale);
    LbRTInfo = new QLabel(QStringLiteral("动态信息"));
    LbRTInfo->setPalette(Pale);
    InitTrainTable();

    VLayTrainInfo = new QVBoxLayout;
    VLayTrainInfo->addLayout(HLayOperat);
    VLayTrainInfo->addWidget(LbStaticInfo);
    VLayTrainInfo->addWidget(TableTrainInfo);
    VLayTrainInfo->addWidget(LbRTInfo);

    ui->tabWidget->currentWidget()->setLayout(VLayTrainInfo);
}

void TrainTicket::InitTrainTable()
{
    TicketHeader  << QStringLiteral("车次") << QStringLiteral("出发地") << QStringLiteral("目的地") <<
                     QStringLiteral("历时") << QStringLiteral("商务/特等") << QStringLiteral("一等座") <<
                     QStringLiteral("二等座") << QStringLiteral("高级软卧") << QStringLiteral("软卧") <<
                     QStringLiteral("动卧") << QStringLiteral("硬卧") << QStringLiteral("软座") <<
                     QStringLiteral("硬座") << QStringLiteral("无座") << QStringLiteral("其他") <<
                     QStringLiteral("备注");
    TableTrainInfo = new QTableWidget(0, TicketHeader.size());
    // 设置车次表表头
    TableTrainInfo->setHorizontalHeaderLabels(TicketHeader);
    // 隐藏列表头
    TableTrainInfo->verticalHeader()->hide();
    // 设置不可编辑
    TableTrainInfo->setEditTriggers(QAbstractItemView::NoEditTriggers);
    // 设置正行选中
    TableTrainInfo->setSelectionBehavior(QAbstractItemView::SelectRows);
//    TableTrainInfo->setSelectionMode(QAbstractItemView::MultiSelection);
    // 隐藏滚动条
    TableTrainInfo->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    // 抢票标签页
    TabRobTicket = new QWidget;
    ui->tabWidget->addTab(TabRobTicket, QStringLiteral("抢票"));
}

// *********************槽函数**************************
void TrainTicket::resizeEvent(QResizeEvent *e)
{
    // 车次信息表列宽
    int SumWidth = 0;
    for(int i = 0; i < TicketHeader.size() - 1; i++) {
        int Width = (int)(TableTrainInfo->width() / TicketHeader.size());
        TableTrainInfo->setColumnWidth(i, Width);
        SumWidth += Width;
    }
    TableTrainInfo->setColumnWidth(TicketHeader.size() - 1, TableTrainInfo->width() - SumWidth - 2);
}

// 登录界面槽函数
void TrainTicket::LogInOutSlot()
{
    // 初始化登录界面
    Log = new JLog;
    // 获取登录信息槽函数
    connect(Log, SIGNAL(SendLogInfo(QString,QString)), this, SLOT(ReceiveLogInfo(QString,QString)));
    // 只是弹出窗口，但是仍然你可以操作其他窗口
//    Log->show();
    // 对话框弹出方式，弹出后无法对其他窗口进行操作
    Log->exec();
    delete Log;
    qDebug() << "Log successful...";
}

void TrainTicket::ReceiveLogInfo(QString Usr, QString Pwd)
{
    User = Usr;
    Passwd = Pwd;
}

// 余票查询按钮槽函数
void TrainTicket::LeftTicketClick()
{
    // 查询前清空队列
    TicketsList.clear();

    // 清除上一次的信息
    int RowCnt = TableTrainInfo->rowCount();
    for(int i = RowCnt; i >= 0; i--)
        TableTrainInfo->removeRow(i);
    for(int i = 0; i < pBtnListOrder.size(); i++) {
        if(pBtnListOrder.at(i) != NULL) {
            delete pBtnListOrder.at(i);
            pBtnListOrder.at(i) = NULL;
        }
    }
    pBtnListOrder.clear();

    QString Src = cBoxSrc->currentText();
    QString Dest = cBoxDest->currentText();
    QString Date = LEdtDate->text();

    if(Src.isEmpty() || Dest.isEmpty() || Date.isEmpty()) {
        QMessageBox::warning(NULL, QStringLiteral("警告"), QStringLiteral("购票信息为空！"),
                             QMessageBox::Ok, QMessageBox::Ok);
        return;
    }
    QMap<QString, QString>::iterator ItSrc = StationMap.find(Src);
    QMap<QString, QString>::iterator ItDest = StationMap.find(Dest);
    // 发送余票查询请求
    QString url = QString("https://kyfw.12306.cn/otn/leftTicket/query?leftTicketDTO.train_date=%3&leftTicketDTO.from_station=%1&leftTicketDTO.to_station=%2&purpose_codes=ADULT").
            arg(ItSrc.value()).arg(ItDest.value()).arg(Date);
    Manage->get(QNetworkRequest(QUrl(url)));
}

void TrainTicket::ReceiveNetworkInfo(QNetworkReply *Info)
{
    QByteArray data = Info->readAll();
    QString str = QString::fromUtf8(data); // data;
    if(str.contains("station_names")) {
        ParseStationInfo(str);
    }
    else if(str.contains("result")){
        ParseTicketInfo(str);
    }
    else if(str.contains("flag") && str.contains("messages")){
        if(ParseIsLogin(str)) {
            IsLog = true;
        }
        else {
            LogInOutSlot();
        }
    }
}

void TrainTicket::CalenderWidget()
{
    if(!IsCalenShow) {
        IsCalenShow = true;
        // 日历控件显示位置
        Calend->setGeometry(this->x() + LEdtDate->x() + 45,
                            this->y() + LEdtDate->y() + LEdtDate->height() + 80,
                            248,164);
        Calend->show();
    }
    else {
        IsCalenShow = false;
        Calend->hide();
    }
}

void TrainTicket::ReceiveCalenDate()
{
    LEdtDate->setText(QDateTime(Calend->selectedDate()).toString("yyyy-MM-dd"));
    Calend->hide();
    IsCalenShow = false;
}

void TrainTicket::AllSeatClick()
{
    if(kBoxAllSeat->isChecked()) {
        kBoxAllSeat->setChecked(true);

        IsSeatCheck(true);
    }
    else {
        kBoxAllSeat->setChecked(false);

        IsSeatCheck(false);
    }
}

void TrainTicket::SubDateClick()
{

}

void TrainTicket::AddDateClick()
{

}

void TrainTicket::CheckStartStationName(QString Info)
{
    disconnect(cBoxSrc, SIGNAL(currentTextChanged(QString)), this, SLOT(CheckStartStationName(QString)));
    cBoxSrc->clear();
    deque<StationName> List;
    GetStationNameList(List, Info);
    int Cnt = List.size();
    cBoxSrc->addItem(Info);
    for(int i = 0; i < Cnt; i++) {
        cBoxSrc->addItem(List.at(i).NameZN);
    }
    connect(cBoxSrc, SIGNAL(currentTextChanged(QString)), this, SLOT(CheckStartStationName(QString)));
}

void TrainTicket::CheckEndStationName(QString Info)
{
    disconnect(cBoxDest, SIGNAL(currentTextChanged(QString)), this, SLOT(CheckEndStationName(QString)));
    cBoxDest->clear();
    deque<StationName> List;
    GetStationNameList(List, Info);
    int Cnt = List.size();
    cBoxDest->addItem(Info);
    for(int i = 0; i < Cnt; i++) {
        cBoxDest->addItem(List.at(i).NameZN);
    }
    connect(cBoxDest, SIGNAL(currentTextChanged(QString)), this, SLOT(CheckEndStationName(QString)));
}

void TrainTicket::OrderTicketsSlot()
{
    if(!IsLog) {
        // 1. 检查是否已经登录，没有登录先登录
        QString url = QString("https://kyfw.12306.cn/otn/login/checkUser");
        Manage->get(QNetworkRequest(QUrl(url)));
    }
    else {
        QPushButton *pBtnOrder = dynamic_cast<QPushButton *>(QObject::sender()); //找到信号发送者
        int Index = pBtnOrder->pos().y() / 30;


        // 2. 预定
        qDebug() << "Order...";

    }
}

// ************************函数****************************
void TrainTicket::IsSeatCheck(bool Check)
{
    kBoxBussiness->setChecked(Check);
    kBoxFrist->setChecked(Check);
    kBoxSecond->setChecked(Check);
    kBoxHSoftSeat->setChecked(Check);
    kBoxSoftPer->setChecked(Check);
    kBoxDSoftper->setChecked(Check);
    kBoxYPer->setChecked(Check);
    kBoxSoftSeat->setChecked(Check);
    kBoxSeat->setChecked(Check);
    kBoxStand->setChecked(Check);
    kBoxOther->setChecked(Check);
}

void TrainTicket::ParseTicketInfo(QString Info)
{
    Info = Info.right(Info.length() - Info.indexOf("|") - 1);
    Info = Info.right(Info.length() - Info.indexOf("|"));
    while(!Info.isEmpty())
    {
        QString Ticket;
        if(Info.indexOf("|") >= 0) {
            Ticket = Info.left(Info.indexOf(","));
            Info = Info.right(Info.length() - Info.indexOf(",") - 1);
            Info = Info.right(Info.length() - Info.indexOf("|") - 1);
        }
        else {
            Ticket = Info;
            Info.clear();
        }
        if(!Ticket.contains("httpstatus") && !Ticket.contains("messages")) {
            TrainInfo TInfo;
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            // 车次
            TInfo.TrainNo = Ticket.left(Ticket.indexOf("|"));
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            // 始发站编码
            TInfo.StartCode = Ticket.left(Ticket.indexOf("|"));
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            // 终点站站编码
            TInfo.EndCode = Ticket.left(Ticket.indexOf("|"));
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            // 开车时间
            TInfo.StartTime = Ticket.left(Ticket.indexOf("|"));
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            // 到站时间
            TInfo.EndTime = Ticket.left(Ticket.indexOf("|"));
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            // 历时时长
            TInfo.SpendTime = Ticket.left(Ticket.indexOf("|"));
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            // 是否可以预定
            TInfo.IsOrder = Ticket.left(Ticket.indexOf("|"));
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            // 高级软卧
            TInfo.GRSleep = Ticket.left(Ticket.indexOf("|"));
            TInfo.GRSleep.replace(" ", "");
            if(TInfo.GRSleep.isEmpty()) TInfo.GRSleep = "--";
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            // 其他
            TInfo.OtherSeat = Ticket.left(Ticket.indexOf("|"));
            TInfo.OtherSeat.replace(" ", "");
            if(TInfo.OtherSeat.isEmpty()) TInfo.OtherSeat = "--";
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            // 软卧
            TInfo.RSleep = Ticket.left(Ticket.indexOf("|"));
            TInfo.RSleep.replace(" ", "");
            if(TInfo.RSleep.isEmpty()) TInfo.RSleep = "--";
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            // 软座
            TInfo.RSeat = Ticket.left(Ticket.indexOf("|"));
            TInfo.RSeat.replace(" ", "");
            if(TInfo.RSeat.isEmpty()) TInfo.RSeat = "--";
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            // 特等座
            TInfo.SSeat = Ticket.left(Ticket.indexOf("|"));
            TInfo.SSeat.replace(" ", "");
            if(TInfo.SSeat.isEmpty()) TInfo.SSeat = "--";
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            // 无座
            TInfo.Stand = Ticket.left(Ticket.indexOf("|"));
            TInfo.Stand.replace(" ", "");
            if(TInfo.Stand.isEmpty()) TInfo.Stand = "--";
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            // 动卧
            TInfo.DSleep = Ticket.left(Ticket.indexOf("|"));
            TInfo.DSleep.replace(" ", "");
            if(TInfo.DSleep.isEmpty()) TInfo.DSleep = "--";
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            // 硬卧
            TInfo.HSleep = Ticket.left(Ticket.indexOf("|"));
            TInfo.HSleep.replace(" ", "");
            if(TInfo.HSleep.isEmpty()) TInfo.HSleep = "--";
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            // 硬座
            TInfo.HSeat = Ticket.left(Ticket.indexOf("|"));
            TInfo.HSeat.replace(" ", "");
            if(TInfo.HSeat.isEmpty()) TInfo.HSeat = "--";
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            // 二等座
            TInfo.SecSeat = Ticket.left(Ticket.indexOf("|"));
            TInfo.SecSeat.replace(" ", "");
            if(TInfo.SecSeat.isEmpty()) TInfo.SecSeat = "--";
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            // 一等座
            TInfo.FirstSeat = Ticket.left(Ticket.indexOf("|"));
            TInfo.FirstSeat.replace(" ", "");
            if(TInfo.FirstSeat.isEmpty()) TInfo.FirstSeat = "--";
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);
            // 商务座
            TInfo.BusSeat = Ticket.left(Ticket.indexOf("|"));
            TInfo.BusSeat.replace(" ", "");
            if(TInfo.BusSeat.isEmpty()) TInfo.BusSeat = "--";
            Ticket = Ticket.right(Ticket.length() - Ticket.indexOf("|") - 1);

            TicketsList.push_back(TInfo);
        }
    }

    // 显示余票信息
    DisplayTicketInfo();
}

void TrainTicket::DisplayTicketInfo()
{
    if(TicketsList.size() <= 0)
        return;

    for(int i = 0; i < TicketsList.size(); i++) {
        QPushButton * pBtnOrder = new QPushButton(QStringLiteral("预定"));
        if(TicketsList.at(i).IsOrder != "Y")
            pBtnOrder->setEnabled(false);
        connect(pBtnOrder, SIGNAL(clicked()), this, SLOT(OrderTicketsSlot()));
        pBtnListOrder.push_back(pBtnOrder);
        QMap<QString, QString>::iterator ItSrc = CodeNameMap.find(TicketsList.at(i).StartCode);
        QMap<QString, QString>::iterator ItDest = CodeNameMap.find(TicketsList.at(i).EndCode);
        TableTrainInfo->insertRow(TableTrainInfo->rowCount());
        TableTrainInfo->setItem(i, 0, new QTableWidgetItem(TicketsList.at(i).TrainNo));
        TableTrainInfo->setItem(i, 1, new QTableWidgetItem(ItSrc.value() + " " + TicketsList.at(i).StartTime));
        TableTrainInfo->setItem(i, 2, new QTableWidgetItem(ItDest.value() + "" + TicketsList.at(i).EndTime));
        TableTrainInfo->setItem(i, 3, new QTableWidgetItem(TicketsList.at(i).SpendTime));
        TableTrainInfo->setItem(i, 4, new QTableWidgetItem(TicketsList.at(i).BusSeat));
        TableTrainInfo->setItem(i, 5, new QTableWidgetItem(TicketsList.at(i).FirstSeat));
        TableTrainInfo->setItem(i, 6, new QTableWidgetItem(TicketsList.at(i).SecSeat));
        TableTrainInfo->setItem(i, 7, new QTableWidgetItem(TicketsList.at(i).GRSleep));
        TableTrainInfo->setItem(i, 8, new QTableWidgetItem(TicketsList.at(i).RSleep));
        TableTrainInfo->setItem(i, 9, new QTableWidgetItem(TicketsList.at(i).DSleep));
        TableTrainInfo->setItem(i, 10, new QTableWidgetItem(TicketsList.at(i).HSleep));
        TableTrainInfo->setItem(i, 11, new QTableWidgetItem(TicketsList.at(i).RSeat));
        TableTrainInfo->setItem(i, 12, new QTableWidgetItem(TicketsList.at(i).HSeat));
        TableTrainInfo->setItem(i, 13, new QTableWidgetItem(TicketsList.at(i).Stand));
        TableTrainInfo->setItem(i, 14, new QTableWidgetItem(TicketsList.at(i).OtherSeat));
        TableTrainInfo->setCellWidget(i, 15, pBtnOrder);
    }
    // 动态信息
    LbRTInfo->setText(QString(QStringLiteral("一共 %1 趟列车")).arg(TicketsList.size()));
}

void TrainTicket::ParseStationInfo(QString Info)
{
    Info = Info.right(Info.length() - Info.indexOf("@") - 1);
    Info = Info.left(Info.indexOf("'"));
    QStringList Station = Info.split("@");
    for(int i = 0; i < Station.size(); i++) {
        if(!Station.isEmpty()) {
            QString str = Station.at(i);
            StationName Name;
            Name.Acronym = str.left(str.indexOf("|"));
            str = str.right(str.length() - str.indexOf("|") - 1);
            Name.NameZN = str.left(str.indexOf("|"));
            str = str.right(str.length() - str.indexOf("|") - 1);
            Name.Code = str.left(str.indexOf("|"));
            str = str.right(str.length() - str.indexOf("|") - 1);
            Name.Spelling = str.left(str.indexOf("|"));
            str = str.right(str.length() - str.indexOf("|") - 1);

            StationList.push_back(Name);
            StationMap.insert(Name.NameZN, Name.Code);
            CodeNameMap.insert(Name.Code, Name.NameZN);
        }
    }
}

int TrainTicket::GetStationNameList(deque<StationName> &List, const QString Reg)
{
    List.clear();
    int Cnt = 0;
    QRegExp Regs(Reg + "\.+");
    for(int i = 0; i < StationList.size(); i++) {
        StationName Name = StationList.at(i);
        if((Name.Acronym.indexOf(Regs) == 0) || (Name.Spelling.indexOf(Regs) == 0) || (Name.NameZN.indexOf(Regs) == 0)) {
            List.push_back(Name);
            ++Cnt;
        }
    }
    return Cnt;
}

bool TrainTicket::ParseIsLogin(QString Info)
{
    Info = Info.right(Info.length() - Info.indexOf("flag") - 1);
    Info = Info.left(Info.indexOf("}"));
    if(Info.contains("true"))
        return true;
    else
        return false;
}

