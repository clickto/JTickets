#ifndef JLINEEDIT_H
#define JLINEEDIT_H

#include <QObject>
#include <QLineEdit>
#include <QMouseEvent>

class JLineEdit : public QLineEdit
{
    Q_OBJECT
public:
    explicit JLineEdit(QLineEdit *parent = 0);

signals:
    void click();

public slots:

protected:
     virtual void mousePressEvent(QMouseEvent *event);
};

#endif // JLINEEDIT_H
